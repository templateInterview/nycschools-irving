package com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.irving.a20230315_irvinggonzalez_nycschools.R

@Composable
fun EmptyView(onBackHome: () -> Unit) {

    Column(modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {

        Card(modifier = Modifier
            .width(350.dp)
            .height(450.dp)
            .shadow(10.dp),
            shape = RoundedCornerShape(16.dp), backgroundColor = Color.LightGray,
        ) {
            Column(modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally){

                Text(text = stringResource(id = R.string.emptyTitle))
                Button(onClick = { onBackHome.invoke() }) {
                    Text(text = stringResource(id = R.string.homeBtn))
                }
            }
        }
    }
}