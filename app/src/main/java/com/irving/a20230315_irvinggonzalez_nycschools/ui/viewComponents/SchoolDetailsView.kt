package com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.irving.a20230315_irvinggonzalez_nycschools.R
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School

@Composable
fun SchoolDetailsView(item: School) {
    Surface(
        shape = RoundedCornerShape(16.dp),
        color = MaterialTheme.colors.primary,
        modifier = Modifier
            .padding(10.dp),
    ) {
        Row(modifier = Modifier.padding(16.dp)) {
            Column(modifier = Modifier
                    .fillMaxSize()
                    .weight(2f),
            ) {
                Spacer(modifier = Modifier.height(4.dp))

                Text(text = item.name,
                    fontSize =  24.sp,
                    style = MaterialTheme.typography.subtitle1,
                    fontWeight = FontWeight.SemiBold, color = Color.White)

                Spacer(modifier = Modifier.height(5.dp))

                Text(text = stringResource(id = R.string.detailLocation, item.location), style = MaterialTheme.typography.subtitle1, color = Color.White)

                Spacer(modifier = Modifier.height(5.dp))

                Text(text = stringResource(id = R.string.detailPhone, item.phone), style = MaterialTheme.typography.subtitle1, color = Color.White)

                Spacer(modifier = Modifier.height(5.dp))

                Text(text = stringResource(id = R.string.detailEmail, item.email), style = MaterialTheme.typography.subtitle1, color = Color.White)

                Spacer(modifier = Modifier.height(5.dp))

                Text(text = stringResource(id = R.string.detailDescription, item.description), style = MaterialTheme.typography.subtitle2, color = Color.White)

            }
        }
    }
}