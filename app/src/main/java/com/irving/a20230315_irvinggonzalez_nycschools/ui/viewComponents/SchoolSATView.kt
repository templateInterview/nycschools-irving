package com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.irving.a20230315_irvinggonzalez_nycschools.R
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.SchoolSAT

@Composable
fun SchoolSATView(item: SchoolSAT){

    Card(modifier = Modifier.padding(10.dp),
        shape = RoundedCornerShape(8.dp),
        backgroundColor = MaterialTheme.colors.primary,
    ) {
        Column(modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally) {

            Text(text = stringResource(id = R.string.satTitle), style = MaterialTheme.typography.body1, color = Color.White)

            Spacer(modifier = Modifier.size(5.dp))

            Text(text = item.name,
                maxLines = 1,
                style = MaterialTheme.typography.body1,
                modifier = Modifier.padding(2.dp), color = Color.White)
        }

        Row(modifier = Modifier
            .height(150.dp)
            .padding(start = 10.dp, end = 10.dp, top = 25.dp, bottom = 5.dp),
            verticalAlignment = Alignment.CenterVertically) {
            RoundedCornerSATCard(modifier = Modifier.weight(1f), stringResource(id = R.string.satScore), item.satTestTaken)
            Spacer(modifier = Modifier.size(5.dp))
            RoundedCornerSATCard(modifier = Modifier.weight(1f), stringResource(id = R.string.mathScore), item.mathScore)
            Spacer(modifier = Modifier.size(5.dp))
            RoundedCornerSATCard(modifier = Modifier.weight(1f), stringResource(id = R.string.readingScore), item.readingScore)
            Spacer(modifier = Modifier.size(5.dp))
            RoundedCornerSATCard(modifier = Modifier.weight(1f), stringResource(id = R.string.writingScore), item.writingScore)
        }
    }
}