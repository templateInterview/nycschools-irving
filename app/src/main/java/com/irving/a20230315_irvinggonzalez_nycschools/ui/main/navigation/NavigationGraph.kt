package com.irving.a20230315_irvinggonzalez_nycschools.ui.main.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.irving.a20230315_irvinggonzalez_nycschools.ui.detail.DetailView
import com.irving.a20230315_irvinggonzalez_nycschools.ui.home.HomeView

@Composable
fun NavigationGraph(){
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Navigation.Home.route){

        composable(route = Navigation.Home.route){
            HomeView(navigateToDetail = {
                navController.navigate(Navigation.Detail.route+"/$it")
            })
        }

        composable(route = Navigation.Detail.route+"/{dbn}",
            arguments = listOf(navArgument("dbn"){ type = NavType.StringType })
        ){
            DetailView(navigateToHome = {
                navController.navigate(Navigation.Home.route)
            })
        }
    }
}