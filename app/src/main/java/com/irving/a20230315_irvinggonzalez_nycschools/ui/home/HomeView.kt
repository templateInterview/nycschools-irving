package com.irving.a20230315_irvinggonzalez_nycschools.ui.home

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School
import com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents.LoadingIndicator
import com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents.SchoolListItem

@Composable
fun HomeView(navigateToDetail: (String) -> Unit) {

    val vm : HomeViewModel = hiltViewModel()
    val state = remember { vm.state }
    val context = LocalContext.current

    val loadingIndicator = remember { vm.loadingIndicator }
    LoadingIndicator(isLoading = loadingIndicator.value)

    when(state.value){
        is HomeViewState.Error -> {
            Toast.makeText(context, (state.value as HomeViewState.Error).error, Toast.LENGTH_LONG).show()
        }
        HomeViewState.Idle -> {
            vm.getListOfSchools()
        }
        HomeViewState.Success -> {
            SchoolList(list = vm.listOfSchools){
                navigateToDetail.invoke(it)
            }
        }
        else -> {
            Toast.makeText(context, state.value.toString(), Toast.LENGTH_LONG).show()
        }
    }
}

@Composable
fun SchoolList(list: List<School>, onClick: (String) -> Unit){

    Box(modifier = Modifier
        .fillMaxSize()
        .wrapContentSize(Alignment.Center)) {
        LazyColumn {
            items(list) {item ->
                SchoolListItem(item = item){
                    onClick.invoke(it)
                }
            }
        }
    }
}