package com.irving.a20230315_irvinggonzalez_nycschools.ui.theme

import androidx.compose.ui.graphics.Color

val secondaryColor = Color(0xFFE86427)
val primaryColor = Color(0xFF0D4EA6)
val defaultColor = Color(0xFF279C9C)
val Teal200 = Color(0xFF03DAC5)