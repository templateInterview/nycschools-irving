package com.irving.a20230315_irvinggonzalez_nycschools.ui.main.navigation

sealed class Navigation(val route: String) {
    object Home : Navigation("Home")
    object Detail : Navigation("Detail")
}