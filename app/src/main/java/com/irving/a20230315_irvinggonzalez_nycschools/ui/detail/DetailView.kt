package com.irving.a20230315_irvinggonzalez_nycschools.ui.detail

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.SchoolSAT
import com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents.EmptyView
import com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents.LoadingIndicator
import com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents.SchoolDetailsView
import com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents.SchoolSATView

@Composable
fun DetailView(navigateToHome: () -> Unit) {

    val vm : DetailViewModel = hiltViewModel()
    val state = remember { vm.state }
    val context = LocalContext.current

    when(state.value){
        is DetailViewState.Error -> {
            Toast.makeText(context, (state.value as DetailViewState.Error).error, Toast.LENGTH_LONG).show()
        }
        DetailViewState.Idle -> {
            vm.getSchoolSAT()
            vm.getSchoolByID()
        }
        DetailViewState.Success -> {
            BodyView(schoolSAT = vm.schoolSAT.value, school = vm.school.value)
        }
        DetailViewState.Empty -> {
            EmptyView { navigateToHome.invoke() }
        }
        else -> {
            Toast.makeText(context, state.value.toString(), Toast.LENGTH_LONG).show()
        }
    }
}

@Composable
fun BodyView(schoolSAT: SchoolSAT?, school: School?){
    Column(modifier = Modifier
        .fillMaxSize()
        .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally) {
        if (schoolSAT != null){
            schoolSAT?.let { SchoolSATView(item = it) }
            school?.let { SchoolDetailsView(item = it) }
        }
    }
}