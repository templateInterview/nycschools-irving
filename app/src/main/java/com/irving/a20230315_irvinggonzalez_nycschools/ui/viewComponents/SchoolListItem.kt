package com.irving.a20230315_irvinggonzalez_nycschools.ui.viewComponents

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.irving.a20230315_irvinggonzalez_nycschools.R
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School

@Composable
fun SchoolListItem(item : School, onClick: (String)-> Unit) {
    Surface(
        shape = RoundedCornerShape(16.dp),
        color = MaterialTheme.colors.primary,
        modifier = Modifier
            .height(210.dp)
            .padding(10.dp),
    ){
        Row(modifier = Modifier.padding(16.dp),
            verticalAlignment = Alignment.CenterVertically) {

            Column(modifier = Modifier
                .fillMaxSize()
                .weight(2f),
                verticalArrangement = Arrangement.Center) {

                Spacer(modifier = Modifier.height(4.dp))

                Text(text = item.name,
                    fontSize = 24.sp,
                    maxLines = 1,
                    style = MaterialTheme.typography.subtitle1,
                    fontWeight = FontWeight.SemiBold)

                Spacer(modifier = Modifier.height(2.dp))

                Text(stringResource(id = R.string.schoolPhone, item.phone))
                Text(stringResource(id = R.string.schoolEmail, item.email))

                Spacer(modifier = Modifier.height(4.dp))

                OutlinedButton(shape = RoundedCornerShape(8.dp),
                    colors = ButtonDefaults.buttonColors(
                        contentColor = Color.Black,
                        backgroundColor = Color.White),
                    onClick = { onClick.invoke(item.dbn) })
                {
                    Text(text = stringResource(id = R.string.schoolListItemBtn),
                        fontSize = 11.sp,
                        fontWeight = FontWeight.SemiBold,
                        style = MaterialTheme.typography.subtitle1)
                }
            }
        }
    }
}