package com.irving.a20230315_irvinggonzalez_nycschools.domain.model

data class SchoolSAT(
    val dbn: String = "",
    val name: String = "",
    val satTestTaken: String = "",
    val readingScore: String = "",
    val mathScore: String = "",
    val writingScore: String = ""
)