package com.irving.a20230315_irvinggonzalez_nycschools.domain.repository

import com.irving.a20230315_irvinggonzalez_nycschools.core.utils.ResponseHandler
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.SchoolSAT

interface ISchoolSATRepository {
    suspend fun getSchoolScores(dbn: String): ResponseHandler<List<SchoolSAT>>
}