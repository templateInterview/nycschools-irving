package com.irving.a20230315_irvinggonzalez_nycschools.data.model

data class SchoolDTO(
    val dbn: String? = "",
    val school_name: String? = "",
    val overview_paragraph: String? = "",
    val location: String? = "",
    val phone_number: String? = "",
    val school_email: String? = "",
)