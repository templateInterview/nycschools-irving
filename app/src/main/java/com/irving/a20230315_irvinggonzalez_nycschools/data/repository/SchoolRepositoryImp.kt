package com.irving.a20230315_irvinggonzalez_nycschools.data.repository

import com.irving.a20230315_irvinggonzalez_nycschools.core.utils.ResponseHandler
import com.irving.a20230315_irvinggonzalez_nycschools.data.mapper.toSchool
import com.irving.a20230315_irvinggonzalez_nycschools.data.service.ISchoolService
import com.irving.a20230315_irvinggonzalez_nycschools.domain.model.School
import com.irving.a20230315_irvinggonzalez_nycschools.domain.repository.ISchoolRepository
import javax.inject.Inject

class SchoolRepositoryImp @Inject constructor(private val service: ISchoolService): ISchoolRepository {

    override suspend fun getAllSchools(): ResponseHandler<List<School>> {
        return try {
            ResponseHandler.Success(service.getSchools(null).map { it.toSchool() })
        } catch (e: Exception){
            ResponseHandler.Error(e.message.toString())
        }
    }

    override suspend fun getSchoolById(dbn: String): ResponseHandler<List<School>> {
        return try {
            ResponseHandler.Success(service.getSchools(dbn).map { it.toSchool() })
        } catch (e: Exception){
            ResponseHandler.Error(e.message.toString())
        }
    }
}